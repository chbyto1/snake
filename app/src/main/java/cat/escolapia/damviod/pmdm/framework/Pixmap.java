package cat.escolapia.damviod.pmdm.framework;

import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

public interface Pixmap {
    public int getWidth();

    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
