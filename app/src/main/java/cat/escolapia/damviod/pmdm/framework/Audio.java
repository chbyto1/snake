package cat.escolapia.damviod.pmdm.framework;

public interface Audio {
    public Music newMusic(String filename);

    public Sound newSound(String filename);
}
