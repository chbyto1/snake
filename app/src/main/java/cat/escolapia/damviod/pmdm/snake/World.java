package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Graphics;

public class World {

    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.02f;

    public Snake snake;
    //Crear 2 diamantes
    public Diamond diamond;
    public Diamond diamond2;
    public boolean gameOver = false;;
    public int score = 0;


    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;
    //Instanciar objetos
    public World() {
        snake = new Snake();
       diamond= placeDiamond();
        diamond2= placeDiamond();
    }

    private Diamond placeDiamond() {
        Diamond d;
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;


            if (diamondX >= WORLD_WIDTH ) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }

        }
        d = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        return d;
    }



    public void update(float deltaTime) {
        boolean check = false;
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            //Colisiones con los diamantes aumenteamos la serpiente
            if(snake.checkXocaDiamant(diamond))
            {
                snake.allarga();
                diamond = placeDiamond();
                score = score + SCORE_INCREMENT;
                tick -= TICK_DECREMENT;
            }
            if(snake.checkXocaDiamant(diamond2))
            {
                snake.allarga();
                diamond2 = placeDiamond();
                score = score + SCORE_INCREMENT;
                tick -= TICK_DECREMENT;
            }
            //Cambiamos el background con los puntos
            if(score >= 50 && check == false)
            {
                check = true;
                Background2(1);
            }
            if(score >= 100 && check == true)
            {
                check = false;
                Background2(2);
            }

        }
    }
    //Modificamos la escore mientras comemos diamantes
    public String getScore()
    {
        int score2 = score;
        return Integer.toString(score2);
    }
    //Metodo del cambio de background
    public void Background2(int num)
    {
        switch (num)
        {
            case 1:
                Assets.background = Assets.background2;
                break;
            case 2:
                Assets.background2 = Assets.background3;
                break;
        }
    }

}
